<?php
namespace controllers;

use utils\Template;
use models\TodoModel;
use controllers\base\Web;
use utils\SessionHelpers;

class TodoWeb extends Web
{
    private $todoModel;
    function __construct()
    {
        $this->todoModel = new TodoModel();
    }

    function liste()
    {
        $todos = $this->todoModel->getTodoUtil(SessionHelpers::getConnected()['IDUTIL']); // Récupération des TODOS présents en base / dans SQL du dossier model.
        Template::render("views/todos/liste.php", array('todos' => $todos)); // Affichage de votre vue.
    }

    function ajouter($texte = "")
    {
        $this->todoModel->ajouterTodo($texte);
        $this->redirect("./liste");
    }

    function terminer($id = ''){
        if($id != ""){
            $this->todoModel->marquerCommeTermine($id);
        }
    
        $this->redirect("./liste");
    }

    function supprimer($id = '')
    {
        if($id != ""){
            $this->todoModel->suppr($id);
        }
    
        $this->redirect("./liste");
    }
}