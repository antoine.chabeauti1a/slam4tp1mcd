--
-- Structure de la table `todos`
--

CREATE TABLE `todos` (
  `id` int(11) NOT NULL,
  `texte` varchar(200) NOT NULL,
  `termine` tinyint(1) NOT NULL DEFAULT 0,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


CREATE TABLE 'utilisateur'(
  'IDUTIL' int(11) NOT NULL AUTO_INCREMENT;
  'LOGINUTIL' varchar(25) NOT NULL;
  'MDPUTIL' varchar(255) NOT NULL;
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `todos`
  ADD 'EMAILUTIL' varchar(100) NOT NULL;

ALTER TABLE 'todos'
  ADD 'IDUTIL' int(11) NOT NULL 
  FOREIGN KEY('IDUTIL') REFERENCES utilisateur(IDUTIL)
