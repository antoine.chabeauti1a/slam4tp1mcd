<?php
namespace models;

use Exception;
use models\base\SQL;

class AuthModel extends SQL
{
    public function __construct()
    {
        parent::__construct('utilisateur', 'LOGINUTIL');
    }

    public function login(string $login, string $mdp): array|null
    {
        $stmt = $this->pdo->prepare("SELECT * FROM utilisateur WHERE LOGINUTIL = ? LIMIT 1");
        $stmt->execute([$login]);
        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $data;

    }

    public function inscr($login, $mail, $mopasse){
        $erreur = "";
        try{
            $stmt = $this->pdo->prepare("INSERT INTO utilisateur (LOGINUTIL, MDPUTIL, EMAILUTIL) VALUES (?, ?, ?)");
            $mdp = password_hash($mopasse, PASSWORD_BCRYPT);
            $stmt->execute([$login, $mdp, $mail]);
        }catch(Exception $e){
            $erreur = $e;
        }
        return $erreur;
    }
}