<?php
namespace models;

use PDO;
use models\base\SQL;
use utils\SessionHelpers;

class TodoModel extends SQL
{
    public function __construct()
    {
        parent::__construct('todos', 'id');
    }

    function marquerCommeTermine($id)
    {
        $stmt = $this->pdo->prepare("UPDATE todos SET termine = 1 WHERE id = ?");
        $stmt->execute([$id]);
    }

    function ajouterTodo($text)
    {
        if($text != "")
        {
            $stmt = $this->pdo->prepare("INSERT INTO todos (texte, IDUTIL) VALUES (?, ?)");
            $stmt->execute([$text, SessionHelpers::getConnected()['IDUTIL']]);
        }
    }

    function suppr($id)
    {

        $stmt = $this->pdo->prepare("DELETE FROM todos WHERE id = ? AND termine = 1");
        $stmt->execute([$id]);
    }

    function getTodoUtil($id){
        $stmt = $this->pdo->prepare("SELECT * FROM todos WHERE todos.IDUTIL = ?");
        $stmt->execute([$id]);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
}