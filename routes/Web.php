<?php

namespace routes;

use routes\base\Route;
use controllers\Account;
use controllers\TodoWeb;
use controllers\VideoWeb;
use utils\SessionHelpers;
use controllers\SampleWeb;
use controllers\AuthControler;

class Web
{
    function __construct()
    {
        $main = new SampleWeb();
        if(SessionHelpers::IsLogin()){
            Route::Add('/about', [$main, 'about']);
            Route::Add('/', [$main, 'home']);
        }
        else{
            Route::Add('/', [$main, 'connexion']);
            Route::Add('/inscription', [$main, 'inscr']);
            Route::Add('/connexion', [$main, 'connexion']);
        }
        
        $todo = new TodoWeb();
        if(SessionHelpers::IsLogin()){
            Route::Add('/todo/liste', [$todo, 'liste']);
            Route::Add('/todo/ajouter', [$todo, 'ajouter']);
            Route::Add('/todo/terminer', [$todo, 'terminer']);
            Route::Add('/todo/supprimer', [$todo, 'supprimer']);
        }
        $aut = new AuthControler();
        Route::Add('/login', [$aut, "login"]);
        Route::Add('/inscr', [$aut, "inscription"]);
        if(SessionHelpers::IsLogin()){
            Route::Add('/deco', [$aut, 'deco']);
        }
    }
    
}

