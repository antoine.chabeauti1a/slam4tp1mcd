<div class="container mt-5">
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h3>Site de démonstration</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, commodi delectus eaque eos esse ex illum laboriosam laborum modi, molestias necessitatibus non provident quidem quis repellat soluta tempore, veritatis vitae.
                </p>
                <div class="text-center">Page générée le <?= $date ?></div>
                <div class="imgDiv"><img src="public/img/dylan-taylor-wG6Ys_86pd8-unsplash.jpg" class="img" alt=""></div>
            </div>
        </div>
    </div>
</div>
